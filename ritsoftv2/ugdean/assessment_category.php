<?php
include("includes/header.php");
include("includes/sidenav.php");
include("includes/connection1.php");
// $dept_name = $_GET['dept_name'];


?>


<div id="page-wrapper">

	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-8">
				<h1 class="page-header">View Assessment Tool Category</h1>
			</div>
			<div class="col-lg-4 text-right">
				<button class="btn btn-primary" style="margin-top: 50px; font-size:large"  data-toggle="modal" data-target="#addCO_modal">
					<span style="margin-right: 10px;"><i class="fa fa-plus"></i></span>Add Category
				</button>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="addCO_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLongTitle">Add Program Outcome</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="">
                                <div class="modal-body">
                                <fieldset class="form-group">
                                        <label for="po">Category Type</label>
			                        	<!--<input class="form-control" type="text" name="category_name" id="category_name" value="" placeholder="Eg: Internal Test" required="required" style="text-transform: capitalize">-->         
			                         <select class="form-control" name="category_type" id="category_type">
                                        <option value="0" selected>Select</option>
                                        <option value="direct" >Direct</option>
                                        <option value="indirect" >Indirect</option>
                                        
                                    </select> 
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label for="po">Category Name</label>
			                        	<input class="form-control" type="text" name="category_name" id="category_name" value="" placeholder="Eg: Internal Test" required="required" style="text-transform: capitalize">         
			                        </fieldset>
                                  
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary" name="add_category" id="add_category">Save </button>
                                </div>
                            </form>
                            <?php
                                if(isset($_POST['add_category']))
                                {
                                    $category_type= $_POST["category_type"];
                                    $category_name= ucfirst($_POST["category_name"]);
                                    $status = 1;

                                    $sql = mysql_query( "insert into accreditation_assessment_tool_category (category_type,category_name,status)VALUES ('$category_type','$category_name','$status')",$con);
                                    if ($sql) {
                                        echo "<script>alert('Succesfully Added')</script>";
                                        echo "<script>window.location.href='assessment_category.php'</script>";
                                    } else {
                                        echo "<script>alert('Failed to Add')</script>";
                                        echo "<script>window.location.href='assessment_category.php'</script>";
                                    
                                    }
                                    
                                    
                                } 
                            
                            ?>
                            
                        </div>
                    </div>
                </div>

		
        <div class="card">
			<div class="card-body">
                <?php
                	$sql_category = mysql_query("select * from accreditation_assessment_tool_category ",$con);
                    $category_count = mysql_num_rows($sql_category);
                    if($category_count>0)
                    {

                    
                ?>
				<table class="table table-success table-bordered table-striped">
					<thead style="font-size:20px;">
						<tr>
                        <th style="text-align: center;" > Category Type </th>
							<th style="text-align: center;" > Category Name </th>

                            <th style="text-align: center;" > Action </th>
						</tr>
					</thead>
					<tbody>
						<?php

                            // $dept_name = $_GET['dept_name'];
							
							$sql_category = mysql_query("select * from accreditation_assessment_tool_category ",$con);
							while($res_category = mysql_fetch_array($sql_category))
    						{
							
					    ?>
							<tr style="font-size:18px;"> 
                                 <td style="text-align: center; "><?php echo $res_category['category_type']; ?> </td>
		    	    		    <td style="text-align: center; "><?php echo $res_category['category_name']; ?> </td>
		    	    		    <td style="text-align: center;">
                                    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#<?php echo $res_category['ass_category_id'] ?>ModalCenter">
										<i class="fa fa-pencil" aria-hidden="true" style="font-size: 18px"></i>
									</button>
                                   
									<a href="delete_assessment_category.php?ass_category_id=<?php echo $res_category['ass_category_id']; ?>">
										<button type="button" class="btn btn-danger">
											<i class="fa fa-trash" aria-hidden="true" style="font-size: 18px"></i>
										</button>
							        </a>
									
								</td>

							</tr>
                            <!-- Modal -->
                            <div class="modal fade" id="<?php echo $res_category['ass_category_id'] ?>ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalLongTitle">Edit Category</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="">
                                            <div class="modal-body">
                                            <fieldset class="form-group">
                                        <label for="po">Category Type</label>
			                        	<!--<input class="form-control" type="text" name="category_name" id="category_name" value="" placeholder="Eg: Internal Test" required="required" style="text-transform: capitalize">-->         
			                        <select name="category_type" id="category_type" value="<?php echo $res_category['category_type']; ?>">
                                        <option value="0" selected>Select</option>
                                        <option value="direct" >Direct</option>
                                        <option value="indirect" >Indirect</option>
                                        
                                    </select> 
                                    </fieldset> 
                                                <fieldset class="form-group">
                                                    <label for="co">Category Name</label>
			                                    	<input class="form-control" type="text" name="category_name" id="category_name" value="<?php echo $res_category['category_name']; ?>" placeholder="Eg: Internal Test" required="required">       
			                                    </fieldset>
                                                
                                            </div>
                                            <input type="hidden" name="ass_category_id" value="<?php echo $res_category['ass_category_id']; ?>">
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="edit" id="edit">Save changes</button>
                                            </div>
                                        </form>
                                        <?php
                                            if(isset($_POST['edit']))
                                            {
                                                // $po_id = $_POST['po_id'];

                                                $ass_category_id = $_POST["ass_category_id"];
                                                $category_type = $_POST["category_type"];
                                                $category_name = $_POST["category_name"];
                                               

                                                
                                                    
                                                $update = mysql_query("update accreditation_assessment_tool_category set category_type='$category_type',category_name='$category_name' where ass_category_id = '$ass_category_id'",$con);
                                                        
                                                
                                                if ($update) {
                                                    echo "<script>alert('Succesfully Updated')</script>";
                                                    echo "<script>window.location.href='assessment_category.php'</script>";
                                                } else {
                                                    echo "<script>alert('Failed to Update')</script>";
                                                    echo "<script>window.location.href='assessment_category.php'</script>";
                                                
                                                }
                                                
                                                
                                            }
                                            
                                        
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
						<?php
				    	    }
				    	?>
					</tbody>
				</table>
                <?php
            
                     }
                     else{

                     
                ?>
                <h3 class="text-center">No Category Available</h3>
                <?php
                     }
                ?>     
			</div>
			
		</div>
        
					
<?php include("includes/footer.php");?>
