<?php
include("includes/header.php");
include("includes/sidenav.php");
include("includes/connection3.php");

$st=$_SESSION['fid'];


$se = true;
?>
<script type="text/javascript"> 
  if(typeof jQuery == 'undefined'){
    document.write('<script src="../dash/vendor/jquery/jquery.min.js"></'+'script>');
  }
</script> 
<title>Attendance</title>
<script>
  function showsub(str)
  {
    var xmlhttp;
    if (window.XMLHttpRequest)
    {
      xmlhttp=new XMLHttpRequest();
    }
    else
    {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.open("GET","get_sub.php?id="+str,true);
    xmlhttp.send();

    xmlhttp.onreadystatechange=function() 
    {
      if(xmlhttp.readyState==4 && xmlhttp.status==200)
      {
        document.getElementById("sub").innerHTML=xmlhttp.responseText;
        showDate ( str) ;
      }
    }
  }
  function getWeightage(){
			
      var subject_id = document.getElementById('subject').value;
      var category_type = document.getElementById('direct_indirect').value;
			var xmlhttp;
			if (window.XMLHttpRequest)
			{
				xmlhttp=new XMLHttpRequest();
			}
			else
			{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.open("GET","get_weightage.php?id="+subject_id+"&category_type="+category_type,true);
			xmlhttp.send();

			xmlhttp.onreadystatechange=function() 
			{
				if(xmlhttp.readyState==4 && xmlhttp.status==200)
				{
          console.log(xmlhttp.responseText);
					document.getElementById("weightage").max = xmlhttp.responseText;
					document.getElementById("weightage_limit").innerHTML ='remaining weightage '+xmlhttp.responseText;

				}
			}
		}
  function get_category(category_type){
			console.log(category_type);
			var xmlhttp;
			if (window.XMLHttpRequest)
			{
				xmlhttp=new XMLHttpRequest();
			}
			else
			{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.open("GET","get_category.php?id="+category_type,true);
			xmlhttp.send();

			xmlhttp.onreadystatechange=function() 
			{
				if(xmlhttp.readyState==4 && xmlhttp.status==200)
				{
          console.log(xmlhttp.responseText);
					document.getElementById("assessment_category_div").innerHTML=xmlhttp.responseText;
				}
			}
		}

</script>

<div id="page-wrapper">
  <link href="jquery-ui.css" rel="stylesheet">
  <script src="jquery.js" type="text/javascript"></script>
  <script src="jquery-ui.js" type="text/javascript"></script>
  
</head>
<body>
  <div class="map_contact">
   <div class="container">

    <h3 class="tittle"></h3>
    <div class="contact-grids" align="center">

     <div class="col-md-8 contact-grid" style="text-align:center">
      <form method="post" enctype="multipart/form-data"><br><br><br>
        <h2>Add Assessment Tool</h2>

        <table  align="center" width="700" class="table table-hover table-bordered">

          <tr><td><label>
          Class</label></td><td>
            <select name="class" onChange="showsub(this.value)" class="form-control">
              <option>select</option>
              <?php
              $c=mysqli_query($con3,"select distinct(classid) from subject_allocation where fid='$st'");

              while($res=mysqli_fetch_array($c))
              {
               $res1=mysqli_query($con3,"select *from class_details where classid='$res[classid]' and active='YES'");
               while($rs=mysqli_fetch_array($res1))
               {
                ?>
                <option value="<?php echo $rs['classid'].",".$rs['courseid'].",S".$rs['semid'].",".$rs['branch_or_specialisation'];?>">
                  <?php echo $rs['courseid'];?>,S<?php echo $rs['semid'];?>,<?php echo $rs['branch_or_specialisation'];?></option>
                  <?php       $dept=$rs['deptname'];
                  $cid=$rs['courseid'];
                  $bs=$rs['branch_or_specialisation'];
                }
              }
              ?>
            </select>

          </td></tr>
          <tr><td><label>Subject</label> </td> <td><div id="sub">
            <select name="sub" class="form-control">
              <option>select</option>
            </select></td>
          </tr>
          <tr>
            <td><label>Assessment Tool Category Type</label> </td> 
            <td>
              <select name="direct_indirect" id="direct_indirect" class="form-control" onchange="get_category(this.value)">
                <option selected disabled>select</option>
                <option value="direct">Direct</option>
                <option value="indirect">Indirect</option>
              </select>
            </td>
          </tr>
          <tr>
            <td><label>Assessment Tool Category</label> </td>
            <td>
              <div id="assessment_category_div">
                <select class="form-control" onchange="getWeightage()" name="tool_category">
                      <option selected>select category</option>
                </select>
              </div>
            </td>
          </tr>

            <tr>
                <td>
                    <label>Name</label> </td>
                <td>
                    <input type="text" class="form-control" name="tool_name">
                </td>
            </tr>
            <tr>
                <td><label>No Of Questions</label></td>
                <td>
                    <input type="number" class="form-control" name="num_of_questions">
                </td>
            </tr>
            <tr>
                <td><label>Weightage(%)</label><br><span id="weightage_limit"></span></td>
                <td>
                    <input type="number" class="form-control" name="weightage" id="weightage" min="1" max="100">
                </td>
            </tr>
            <tr>
                <td><label>Target(%)</label></td>
                <td>
                    <input type="number" class="form-control" name="target" min="1" max="100">
                </td>
            </tr>
       </table>

  <input type="submit" name="add_tool"  value="Add Tool" class="btn btn-primary" />
  
</form>

<?php
    if(isset($_POST['add_tool']))
    {
        $class=explode(",",$_POST['class']);
        $class_name = $class[2].' '.$class[1];
        $subject_id=explode("-",$_POST['sub']);
        $category_id=$_POST['tool_category'];
        $name=$_POST['tool_name'];
        $no_of_questions=$_POST['num_of_questions'];
        $weightage=$_POST['weightage'];
        $target=$_POST['target'];
        $status = 1;
        $sql = mysql_query( "insert into accreditation_assesment_tool(class_id,subjectid,category_id,name,no_of_questions,weightage,target_percentage,status)VALUES ('$class_name','$subject_id[0]','$category_id','$name','$no_of_questions',$weightage,$target,'$status')",$con);
        if ($sql) {
            echo "<script>alert('Succesfully Added')</script>";
            echo "<script>window.location.href='assesment_tool.php'</script>";
        } else {
            echo "<script>alert('Failed to Add')</script>";
            echo "<script>window.location.href='assesment_tool.php'</script>";
        
        }
    }
?>
</body>
</html>
<?php include("includes/footer.php");   ?>
