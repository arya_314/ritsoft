<?php 
    include("includes/header.php");
    include("includes/sidenav.php");
    include("includes/connection3.php");
    
    $st=$_SESSION['fid'];
        
?>
<div id="page-wrapper">

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-8">
            <h1 class="page-header">View Assessment Tool</h1>
        </div>
    </div>
    

    <?php
        $assesment_tool_sel = mysql_query("select * from accreditation_assesment_tool inner join subject_allocation on accreditation_assesment_tool.subjectid = subject_allocation.subjectid where fid='$st'",$con);

        $at_count = mysql_num_rows($assesment_tool_sel);

        if($at_count == 0)
        {
    ?>
    <div class="text-center">
            <h3>No Assessment Tool Availabe</h3>

    </div>
    <?php
        }
        else{
    ?>

    <div class="card">
        <div class="card-body">
            <table class="table table-success table-bordered table-striped">
                <thead style="font-size:20px;">
                    <tr>
                        <th style="text-align: center;" > Class </th>
                        <th style="text-align: center;" > Subject </th>
                        <th style="text-align: center;" > Assessment Tool Category</th>
                        <th style="text-align: center;" > Name</th>
                        <th style="text-align: center;" > No of Questions</th>
                        <th style="text-align: center;" > Weightage</th>
                        <th style="text-align: center;" > Target(%)</th> 
                        <th style="text-align: center;" > Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                        // $dept_name = $_GET['dept_name'];
                        
                        $assesment_tool_sel = mysql_query("select * from accreditation_assesment_tool inner join subject_allocation on accreditation_assesment_tool.subjectid = subject_allocation.subjectid inner join accreditation_assessment_tool_category on accreditation_assesment_tool.category_id=accreditation_assessment_tool_category.ass_category_id where fid='$st'",$con);
                        while($res_at = mysql_fetch_array($assesment_tool_sel))
                        {
                        
                    ?>
                        <tr style="font-size:18px;"> 
                            <td style="text-align: center; "><?php echo $res_at['class_id']; ?> </td>
                            <td style="text-align: center; "><?php echo $res_at['subjectid']; ?> </td>
                            <td style="text-align: center;"><?php echo $res_at['category_name']; ?></td>
                            <td style="text-align: center;"><?php echo $res_at['name']; ?></td>
                            <td style="text-align: center;"><?php echo $res_at['no_of_questions']; ?></td>
                            <td style="text-align: center;"><?php echo $res_at['weightage']; ?></td>
                            <td style="text-align: center;"><?php echo $res_at['target_percentage']; ?></td>
                            <td style="text-align: center;">
                                <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#<?php echo $res_at['tool_id'] ?>ModalCenter">
                                    <i class="fa fa-pencil" aria-hidden="true" style="font-size: 18px"></i>
                                </button>
                                <a href="delete_assessment_tool.php?tool_id=<?php echo $res_at['tool_id']; ?> ">
                                    <button type="button" class="btn btn-danger">
                                        <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px"></i>
                                    </button>
                                </a>
                                
                            </td>

                        </tr>
                        <!-- Modal -->
                        <div class="modal fade" id="<?php echo $res_at['tool_id'] ?>ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalLongTitle">Edit Assessment Tool</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="">
                                            <div class="modal-body">
                                                <fieldset class="form-group">
                                                    <label for="co">Class</label>
			                                    	<input class="form-control" type="text" value="<?php echo $res_at['class_id']; ?> " readonly>         
			                                    </fieldset>
                                                <fieldset class="form-group">
                                                    <label for="co">Subject</label>
			                                    	<input class="form-control" type="text" value="<?php echo $res_at['subjectid']; ?> " readonly>         
			                                    </fieldset>
			                                    <fieldset class="form-group">
			                                    	<label for="co_name">Assessment Tool Category</label>
                                                    <input class="form-control" type="text" value="<?php echo $res_at['category_name']; ?>" readonly>         
			                                    </fieldset>
                                                <fieldset class="form-group">
			                                    	<label for="co_name">Name</label>
                                                    <input class="form-control" type="text" value="<?php echo $res_at['name']; ?>" name="name">         
			                                    </fieldset>
                                                <fieldset class="form-group">
			                                    	<label for="co_name">No OF Question</label>
                                                    <input class="form-control" type="text" value="<?php echo $res_at['no_of_questions']; ?>" name="no_of_questions">         
			                                    </fieldset>
                                                <fieldset class="form-group">
			                                    	<label for="co_name">Weightage</label>
                                                    <input class="form-control" type="text" value="<?php echo $res_at['weightage']; ?>" name="weightage">         
			                                    </fieldset>
                                                <fieldset class="form-group">
			                                    	<label for="co_name">Target(%)</label>
                                                    <input class="form-control" type="text" value="<?php echo $res_at['target_percentage']; ?>" name="target_percentage">         
			                                    </fieldset>
                                            </div>
                                            <input type="hidden" name="tool_id" value="<?php echo $res_at['tool_id']; ?>">
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="submit" id="submit">Save changes</button>
                                            </div>
                                        </form>
                                        <?php
                                            if(isset($_POST['submit']))
                                            {
                                                $tool_id = $_POST['tool_id'];
                                                $name = $_POST["name"];
                                                $no_of_questions = $_POST["no_of_questions"];
                                                $weightage = $_POST["weightage"];
                                                $target_percentage	= $_POST["target_percentage"];

                                                $update = mysql_query("update accreditation_assesment_tool set name='$name', no_of_questions='$no_of_questions',weightage='$weightage',target_percentage='$target_percentage' WHERE tool_id='$tool_id'",$con);
                                                if ($update) {
                                                    echo "<script>alert('Succesfully Updated')</script>";
                                                    echo "<script>window.location.href='view_assesment_tool.php'</script>";
                                                } else {
                                                    echo "<script>alert('Failed to Update')</script>";
                                                    echo "<script>window.location.href='view_assesment_tool.php'</script>";
                                                
                                                }
                                            } 
                                        
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
                       
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            
        </div>
        <?php 
            }			
        ?>
    </div>
    
                
<?php include("includes/footer.php");?>
