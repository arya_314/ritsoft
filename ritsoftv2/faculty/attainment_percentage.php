

<?php
include("includes/header.php");
include("includes/sidenav.php");
include("includes/connection3.php");

$st=$_SESSION['fid'];



?>


<div id="page-wrapper">
  <link href="jquery-ui.css" rel="stylesheet">
  <script src="jquery.js" type="text/javascript"></script>
  <script src="jquery-ui.js" type="text/javascript"></script>
  
</head>
<body>
  <div class="map_contact">
   <div class="container">

    <h3 class="tittle"></h3>
    <div class="contact-grids" align="center">

     <div class="col-md-6 contact-grid" style="text-align:center">
      <!-- <form method="post" enctype="multipart/form-data"><br><br><br> -->
        <div class="row">
            <div class="col-md-6">
                <h2>View CO Level</h2>
            </div>
           
            <div class="col-md-3" style="margin-top:20px;">
                <button class="btn btn-primary" data-toggle="modal" data-target="#edit_modal">Edit CO Level</button>
            </div>
        </div>
        <hr>
        <!-- Modal -->
		<div class="modal fade" id="addCOlevel_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLongTitle">Add Program Outcome</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="">
                        <div class="modal-body">
                            <fieldset class="form-group">
                                <label>Percentage marks for attainment level 1</label>
		                    	<input type="number" class="form-control" name="level_one" min="1" max="100" required>         
		                    </fieldset>
                            <fieldset class="form-group">
                                <label>Percentage marks for attainment level 2</label>
		                    	<input type="number" class="form-control" name="level_two" min="1" max="100" required>         
		                    </fieldset>
		                    <fieldset class="form-group">
		                    	<label>Percentage marks for attainment level 3</label>
                                <input type="number" class="form-control" name="level_three" min="1" max="100" required>
		                    </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" name="add_colevel" id="add_colevel">Save changes</button>
                        </div>
                    </form>
                    <?php
                        if(isset($_POST['add_colevel']))
                        {
                            $level_one=$_POST['level_one'];
                            $level_two=$_POST['level_two'];
                            $level_three=$_POST['level_three'];
                            $status = 1;
                            $sql = mysql_query( "insert into accreditation_co_level(level_one,level_two,level_three,status)VALUES ('$level_one','$level_two','$level_three','$status')",$con);
                            if ($sql) {
                                echo "<script>alert('Succesfully Added')</script>";
                                echo "<script>window.location.href='attainment_percentage.php'</script>";
                            } else {
                                echo "<script>alert('Failed to Add')</script>";
                                echo "<script>window.location.href='attainment_percentage.php'</script>";
                            
                            }
                        } 
                    
                    ?>
                    
                </div>
            </div>
        </div>
        <!-- Modal -->
		<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLongTitle">Edit CO Level</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="">
                        <div class="modal-body">
                        <?php
                            $get_attainment_level = mysql_query("select * from accreditation_co_level", $con);
                            while($res_attainment_level = mysql_fetch_array($get_attainment_level)){
                        ?>
                            <fieldset class="form-group">
                                <label>Percentage marks for attainment level 1</label>
		                    	<input type="number" class="form-control" name="level_one" min="1" max="100" value="<?php echo $res_attainment_level[1]; ?>" required>         
		                    </fieldset>
                            <fieldset class="form-group">
                                <label>Percentage marks for attainment level 2</label>
		                    	<input type="number" class="form-control" name="level_two" min="1" max="100" value="<?php echo $res_attainment_level[2]; ?>" required>         
		                    </fieldset>
		                    <fieldset class="form-group">
		                    	<label>Percentage marks for attainment level 3</label>
                                <input type="number" class="form-control" name="level_three" min="1" max="100" value="<?php echo $res_attainment_level[3]; ?>" required>
		                    </fieldset>
                        </div>
                        <input type="hidden" name="co_level_id" value="<?php echo $res_attainment_level[0]; ?>">
                        <?php 
                            }
                        ?>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" name="edit_colevel" id="edit_colevel">Save changes</button>
                        </div>
                    </form>
                    <?php
                        if(isset($_POST['edit_colevel']))
                        {
                            $level_one=$_POST['level_one'];
                            $level_two=$_POST['level_two'];
                            $level_three=$_POST['level_three'];
                            $co_level_id=$_POST['co_level_id'];

                            $update = mysql_query("update accreditation_co_level set level_one='$level_one',level_two='$level_two',level_three='$level_three' where co_level_id='$co_level_id' ",$con);

                            if ($update) {
                                echo "<script>alert('Succesfully Edited')</script>";
                                echo "<script>window.location.href='attainment_percentage.php'</script>";
                            } else {
                                echo "<script>alert('Failed to Edit')</script>";
                                echo "<script>window.location.href='attainment_percentage.php'</script>";
                            
                            }
                        } 
                    
                    ?>
                    
                </div>
            </div>
        </div>
        <table  align="center" width="700" class="table table-hover table-bordered">

            <?php
                $get_attainment_level = mysql_query("select * from accreditation_co_level", $con);
                while($res_attainment_level = mysql_fetch_array($get_attainment_level)){
            ?>
            <tr>
                <td>
                    <label>Percentage  marks for attainment level 1</label> 
                </td>
                <td>
                    <?php echo $res_attainment_level[1].'%'; ?>
                </td>
                <!-- <td>
                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#level1_modal"><i class="fa fa-pencil"></i></button>
                </td> -->

            </tr>
            
            <tr>
                <td><label>Percentage  marks for attainment level 2</label></td>
                <td>
                    <?php echo $res_attainment_level[2].'%'; ?>
                    <!--  -->
                </td>
                <!-- <td>
                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#level2_modal"><i class="fa fa-pencil"></i></button>
                </td> -->
            </tr>
            
            <tr>
                <td><label>Percentage  marks for attainment level 3</label></td>
                <td>
                    <?php echo $res_attainment_level[3].'%'; ?>
                    <!--  -->
                </td>
                <!-- <td>
                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#level3_modal"><i class="fa fa-pencil"></i></button>
                </td> -->
            </tr>
            
            <?php 
                }
            ?>
            
       </table>

  <!-- <input type="submit" name="add_co_level"  value="Add" class="btn btn-primary" /> -->
  


</body>
</html>
<?php include("includes/footer.php");   ?>
