<?php
include("includes/header.php");
include("includes/sidenav.php");
?>

<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><span style="font-weight:bold;">TRANSFER CERTIFICATE
			</span></h1>
		</div>
	</div>
	
	

	<form id="form1" name="form1" method="post" action="tc_pdf_template.php" enctype="multipart/form-data" class="sear_frm" target="_blank" >
		
		
		

		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="tc_no">TC No</label>
				<input type="text" class="form-control"  name="tc_no" id="tc_no" value=""  required>
			</div>
			<div class="form-group col-md-6">
				<label  for="adno">Admission No</label>
				<input type="text" class="form-control" name="adno" id="adno" value="" >
				
			</div>
		</div>

		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="name">Name Of Student</label>
				<input type="text" class="form-control"  name="name" id="name" value=""  required>
			</div>
			<div class="form-group col-md-6">
				<label  for="dob">Data Of Birth</label>
				<input type="date" class="form-control" name="dob" id="dob" value="" required>
				
			</div>
		</div>

<!-- should be removed after correcting stored procedure.... -->
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="dobw">Date of Birth (in words)</label>
				<input type="text" class="form-control"  name="dobw" id="dobw" value=""  required>
			</div>
		</div>



		
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="caste">Caste & Religion</label>
				<input type="text" class="form-control"  name="caste" id="caste" value=""  required>
			</div>
			<div class="form-group col-md-6">
				<label  for="dte">Year Of Admission</label>
				<input type="text" class="form-control" name="dte" id="dte" value="" >
				
			</div>
		</div>
		
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="class">Classes to which Admitted</label>
				<input type="text" class="form-control"  name="class" id="class" value="" required>
			</div>
			<div class="form-group col-md-6">
				<label  for="leaving">Date of Leaving</label>
				<input type="date" class="form-control" name="leaving" id="leaving" value="" required>
				
			</div>
		</div>
		
		
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="relive">Classes from which Relived</label>
				<input type="text" class="form-control"  name="relive" id="relive" value="" required>
			</div>
			<div class="form-group col-md-6">
				<label  for="higher">Whether qualified for promotion to higher class</label>
				<input type="text" name="higher" id="higher" required class="form-control" >
				
				
			</div>
		</div>
		
		
		<div class="form-row">
			
			<div class="form-group col-md-6">
				<label  for="fee">Whether all the fees and other due have been paid</label>
				<select name="fee" id="fee" required class="form-control" >
					<option selected>----Choose----</option>
					<option value="No" >No</option>
                                        <option value="Yes" >Yes</option>
                                        <option value="NA" >NA</option>

				</select>
				
			</div>
			
			<div class="form-group col-md-6">
				<label  for="concession">Whether the student was receipt of the fee concession</label>
				<select class="form-control" name="concession" id="concession"  required class="form-control" >
					<option selected>----Choose----</option>
                                        <option value="No" >No</option>
					<option value="Yes" >Yes</option>
                                        <option value="NA" >NA</option>

				</select>
				
			</div>
		</div>
		
		
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="application">Date of application of TC</label>
				<input type="date" class="form-control" name="application" id="application" value="" required>
			</div>
			<div class="form-group col-md-6">
				<label for="issue">Date of Issue of TC</label>
				<input type="date" class="form-control" name="issue" id="issue"  value=""  class="form-control">
				
			</div>
		</div>
		<!-- <input type="hidden" name="id" id="id"  value="<?php //echo $id?>" /> -->
		
		<div class="form-row">
			<div class="form-group col-md-6">
				<label or="reason">Reason for Leaving</label>
				<input name="reason" class="form-control" id="reason" value="" required>
			</div>
			<div class="form-group col-md-6">
				<label  for="institution">Institution to which the student intends proceeding </label>
				<input type="text" max="44" class="form-control" name="institution" id="institution" class="form-control">
				
			</div>
		</div>
	<!--	<input type="hidden" name="classid" id="classid"  value="<?php // echo $cid?>" />
		<input type="hidden" name="studid" id="studid"  value="<?php //echo $sid?>" />
		<input type="hidden" name="rollno" id="rollno"  value="<?php //echo $rlno?>" /> -->

		
		<div align="center">
			<input type="submit" name="submit" id="submit" value="PRINT TC" class="btn btn-primary" />
			<input type="reset" name="submit2" id="submit2" value="CLEAR"  class="btn btn-primary" />
			
		</div>
		
	</form>
</div>
</html>

<?php
	// Link for footer.php
include("includes/footer.php");
?>
