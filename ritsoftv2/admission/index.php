<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="formstyle.css">

</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
      // RELOAD PAGE ON BUTTON CLICK EVENT.
        $('#reload_page').click(function () {
            location.reload(true); 
        });
    });
</script>
<script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>
<body>

<div> <img style="margin-left: auto; display: block; margin-right: auto;" src="LOGO.png" alt="" width="1200px" height="120px"> 
<h2 style="color:green; text-align: center;"> ADMISSION DATA ENTRY PORTAL - 2021</h2> </div>
<div style="float:left; width:450px; height:400px; ">
<h2 style="color:red;">******* Instructions ******* </h2>
<ul style="list-style-type: square;text-align:justify;line-height:180%; font-family: Helvetica, Arial, sans-serif;
  font-size: 17px;">
  <li>Students should have a valid Email Id for registration
  </li>
  <li>Once the information is successfully submitted, candidates can download their 
  registration form. </li>
  <li>Please keep the temporary number on the registration form for future reference.</li>
  <li>Please see the Personal and Academic details needed for registration.<a href="instructions.pdf" target="blank">Click Here</a></li>
  <li>Generate Application Form, if already registered.<a href="../stud-application.php" target="blank">Click Here</a></li>
</ul>

</div>
<div style="float:right">
<form action="action.php" method="post">
<div class="container">
    <label for="psw"><b><span style="font-size: 15px">Enter Your Email ID</span></b></label>
    <input class ="fstyle" type="email" placeholder="Enter Emailid" name="emailid" required>
    <label for="psw"><b><span style="font-size: 15px">Enter Captcha Code</span></b></label>
    <input  class ="fstyle" type="text" class="form-control" name="captcha" id="captcha" required>
        <img id="captcha_img" src="captcha.php" width="200px" height="70px" alt="PHP Captcha"><br>
        Unable to read Captcha 
        <input type="button" id="reload_page" style=" margin:1px;width:150px;height:30px;font-size: 18px" value="Refresh Captcha"/> <br><br>

    <button name="submit" type="submit">REGISTER</button>
   
   </div>
   </form>
 <div>

</body>
</html>
