<?php
  include("includes/connection1.php");
  include("includes/header.php");
  include("includes/sidenav.php");
  ?> 
  <!DOCTYPE html>
  <html>
    <head>
      <title>
      datasheet
      </title>
    </head>
    <body>
      <div id="page-wrapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
              <h1 class="page-header">Data Sheet</h1>
            </div>
          </div>
          <form method="post">

            <?php
            $tei = 0; //..............store teaching effectiveness index
    //if(isset($_POST["go"]))
    //{
            $subjectid=$_GET['subid'];
            $ay=$_GET['ay'];
            $fid=$_GET['fid'];
            $classid1=$_GET['classid1'];

            $l1=mysql_query("select * from class_details where classid='$classid1'") or die(mysql_error());
      			$r1=mysql_fetch_assoc($l1);
      			$deptname1=$r1["deptname"];
            $semid1=$r1["semid"];




//current date
            $l=mysql_query("select acd_year from academic_year where status=1") or die(mysql_error());
			      $r=mysql_fetch_assoc($l);
			      $prev=$r["acd_year"];
//.........number of students responded based on subject
            $result=mysql_query("SELECT COUNT(responseid) as cnt FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay'",$con);
            if($result)
            {
              while($row=mysql_fetch_assoc($result))
              {

                ?>
                <p align="right">
                  <input onclick="window.location.href='feedback_result.php'" type="submit" class="btn btn-primary" name="submit" value="Back"/>
                </p>
                <?php
                if(isset($_POST['submit']))
                {
                  echo "<script>window.location.href='feedback_result.php'</script>";
                }
                ?>
                <label>Number of students:</label>
                <input type = "text" class="form-control" name = "nostudents" value='<?php echo $row['cnt']?>' disabled />
                </div>
                <?php
                 $n = $row['cnt'];
                ?>
            </p>
         <?php
       }     
     }
     ?>
     <style>
     table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: middle;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #dddddd;
    }
  </style>
  <div class="row">
    <div class="col-lg-12">

     <div class=".col-sm-6 .col-md-5 .col-md-offset-2 .col-lg-6 .col-lg-offset-0">
      <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
        <tr>
          <th rowspan="2">Question</th>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
        </tr>
        
        <tr>
          <th colspan="5">Count of Students</th>
        </tr>
        

        <tr>
          <td rowspan="2">1. Was the course objectives made clear by the teacher during the commencement of the course?</td>
          <td>Yes</td>
          <td>No</td>
          <td>Not Sure</td>
          <td></td>
        </tr>



        <tr>
          <td>

              <?php
  //.............number of students responded for question1 with different responses
              $co1=mysql_query("SELECT COUNT(responseid) as cob1 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q1='op1'",$con);
              if($co1)
              {
                while($corow1=mysql_fetch_assoc($co1))
                {
                 echo $corow1['cob1'];
                 $a1 = $corow1['cob1'];
               }
             }
             ?>

           </td>
           <td>
            <?php
            $co2=mysql_query("SELECT COUNT(responseid) as cob2 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q1='op2'",$con);
            if($co2)
            {
              while($corow2=mysql_fetch_assoc($co2))
              {
               echo $corow2['cob2'];
               $a2 = $corow2['cob2'];
             }
           }
           ?>  
         </td>
         <td>
          <?php
          $co3=mysql_query("SELECT COUNT(responseid) as cob3 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q1='op3'",$con);
          if($co3)
          {
            while($corow3=mysql_fetch_assoc($co3))
            {
             echo $corow3['cob3'];
             $a3 = $corow3['cob3'];
           }
          }
          ?>  
        </td>
        <td></td>
        
      </tr>
     
<tr>
  <td rowspan="2">2. Were there any classes you missed due to network issues?</td>
  <td>Yes</td>
  <td>No</td>
  <td></td>
  <td></td>
</tr>
<tr>
  <td>
    <?php
//.............number of students responded for question 2 with different responses

    $cm1=mysql_query("SELECT COUNT(responseid) as clm1 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q2='op1'",$con);
    if($cm1)
    {
      while($cmrow1=mysql_fetch_assoc($cm1))
      {
       echo $cmrow1['clm1'];
       $a1 = $cmrow1['clm1'];
     }
   }
   ?>  
 </td>
 <td>
  <?php
  $cm2=mysql_query("SELECT COUNT(responseid) as clm2 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q2='op2'",$con);
  if($cm2)
  {
    while($cmrow2=mysql_fetch_assoc($cm2))
    {
     echo $cmrow2['clm2'];
     $a2 = $cmrow2['clm2'];
   }
 }
 ?>  
</td>
<td></td>
<td></td>
</tr>

<tr>
  <td rowspan="2">2 a)If so how many?</td>
  <td>1 - 5</td>
  <td>6 - 10</td>
  <td>11 - 15</td>
  <td>More than 15</td>
</tr>
<tr>
  <td>
    <?php
//.............number of students responded for question 3 with different responses

    $hw1=mysql_query("SELECT COUNT(responseid) as hwm1 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q21='op1'",$con);
    if($hw1)
    {
      while($hwrow1=mysql_fetch_assoc($hw1))
      {
       echo $hwrow1['hwm1'];
       $a1 = $hwrow1['hwm1'];
     }
   }
   ?>  
 </td>
 <td>
  <?php
  $hw2=mysql_query("SELECT COUNT(responseid) as hwm2 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q21='op2'",$con);
  if($hw2)
  {
    while($hwrow2=mysql_fetch_assoc($hw2))
    {
     echo $hwrow2['hwm2'];
     $a2 = $hwrow2['hwm2'];
   }
 }
 ?>  
</td>
<td>
  <?php
  $hw3=mysql_query("SELECT COUNT(responseid) as hwm3 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q21='op3'",$con);
  if($hw3)
  {
    while($hwrow3=mysql_fetch_assoc($hw3))
    {
     echo $hwrow3['hwm3'];
     $a3 = $hwrow3['hwm3'];
   }
 }
 ?>  
</td>
<td>
  <?php
  $hw4=mysql_query("SELECT COUNT(responseid) as hwm4 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q21='op4'",$con);
  if($hw4)
  {
    while($hwrow4=mysql_fetch_assoc($hw4))
    {
     echo $hwrow4['hwm4'];
     $a4 = $hwrow4['hwm4'];
   }
 }
 ?>  
</td>
</tr>



<tr>
  <td rowspan="2">2 b)How did you manage to make up the loss?</td>
  <td>By Watching recorded class</td>
  <td>With the help of notes shared by the teacher </td>
  <td>self study</td>
  <td></td>
</tr>
<tr>
  <td>
    <?php
//.............number of students responded for question 4 with different responses

    $en1=mysql_query("SELECT COUNT(responseid) as cen1 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q22='op1'",$con);
    if($en1)
    {
      while($enrow1=mysql_fetch_assoc($en1))
      {
       echo $enrow1['cen1'];
       $a1 = $enrow1['cen1'];
     }
   }
   ?>  
 </td>
 <td>
  <?php
  $en2=mysql_query("SELECT COUNT(responseid) as cen2 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q22='op2'",$con);
  if($en2)
  {
    while($enrow2=mysql_fetch_assoc($en2))
    {
     echo $enrow2['cen2'];
     $a2 = $enrow2['cen2'];
   }
 }
 ?>  
</td>
<td>
  <?php
  $en3=mysql_query("SELECT COUNT(responseid) as cen3 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q22='op3'",$con);
  if($en3)
  {
    while($enrow3=mysql_fetch_assoc($en3))
    {
     echo $enrow3['cen3'];
     $a3 = $enrow3['cen3'];
   }
 }
 ?>  
</td>
<td></td>
</tr>



<tr>
  <td rowspan="2">3. Did the teacher dictate notes online and made you write down the notes? </td>
  <td>Yes</td>
  <td>No</td>
  <td>Sometimes</td>
  <td></td>
</tr>
<tr>

  <td>
    <?php
//.............number of students responded for question 5 with different responses

    $d1=mysql_query("SELECT COUNT(responseid) as cd1 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q3='op1'",$con);
    if($d1)
    {
      while($drow1=mysql_fetch_assoc($d1))
      {
       echo $drow1['cd1'];
       $a1 = $drow1['cd1'];
     }
   }
   ?>  
 </td>
 <td>
  <?php
  $d2=mysql_query("SELECT COUNT(responseid) as cd2 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q3='op2'",$con);
  if($d2)
  {
    while($drow2=mysql_fetch_assoc($d2))
    {
     echo $drow2['cd2'];
     $a2 = $drow2['cd2'];
   }
 }
 ?>  
</td>
<td>
  <?php
  $d3=mysql_query("SELECT COUNT(responseid) as cd3 FROM online_feedback WHERE subjectid='$subjectid' and fid='$fid' and deptname='$deptname1' and semid='$semid1' and acdyear='$ay' and q3='op3'",$con);
  if($d3)
  {
    while($drow3=mysql_fetch_assoc($d3))
    {
     echo $drow3['cd3'];
     $a3 = $drow3['cd3'];
    }
  }
 ?>  
</td>
<td></td>
</tr>


</table>
</form>
</body>
</div>
<!-- /.row -->
</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper --> 
</html>
<br>
<br>
<?php
include("includes/footer.php");
?>
