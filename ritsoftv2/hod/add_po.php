<?php
include("includes/header.php");
include("includes/sidenav.php");
include("includes/connection1.php");
$dept_name = $_GET['dept_name'];


?>


<div id="page-wrapper">

	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-8">
				<h1 class="page-header">View Program Outcome</h1>
			</div>
			<div class="col-lg-4 text-right">
				<button class="btn btn-primary" style="margin-top: 50px; font-size:large"  data-toggle="modal" data-target="#addCO_modal">
					<span style="margin-right: 10px;"><i class="fa fa-plus"></i></span>Add PO
				</button>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="addCO_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLongTitle">Add Program Outcome</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="">
                                <div class="modal-body">
                                    <fieldset class="form-group">
                                        <label for="po">PO Code</label>
			                        	 <input class="form-control" type="text" name="po_code" id="po_code" value="" placeholder="Eg: PO1" required="required" style="text-transform: uppercase">          
			                        </fieldset>
                                    <fieldset class="form-group">
                                        <label for="po">PO Title</label>
			                        	<input class="form-control" type="text" name="po_title" id="po_title" value="" placeholder="" required="required" style="text-transform: uppercase">         
			                        </fieldset>
			                        <fieldset class="form-group">
			                        	<label for="po_description">PO Description</label>
                                        <textarea class="form-control" name="po_description" id="po_description" style="height:50px;" required style="text-transform: capitalize"></textarea>
			                        </fieldset>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary" name="add_po" id="add_po">Save changes</button>
                                </div>
                            </form>
                            <?php
                                if(isset($_POST['add_po']))
                                {
                                    $po_code = strtoupper($_POST["po_code"]);
                                    $po_title = strtoupper($_POST["po_title"]);
                                    $po_description = ucfirst($_POST["po_description"]);
									$status = 1;
									$sql = mysql_query( "insert into program_outcome(po_code,po_title,po_description,dept_name,po_status)VALUES ('$po_code','$po_title','$po_description','$dept_name','$status')",$con);
                                    if ($sql) {
                                        echo "<script>alert('Succesfully Added')</script>";
                                        echo "<script>window.location.href='add_po.php?dept_name=$dept_name'</script>";
                                    } else {
                                        echo "<script>alert('Failed to Add')</script>";
                                        echo "<script>window.location.href='add_po.php?dept_name=$dept_name'</script>";
                                    
                                    }
                                } 
                            
                            ?>
                            
                        </div>
                    </div>
                </div>

		<?php
			$po_count_sel = mysql_query("select * from program_outcome where dept_name = '$dept_name'",$con);

			$po_count = mysql_num_rows($po_count_sel);

			if($po_count == 0)
			{
		?>
		<div class="text-center">
				<h3>No Program Outcome Availabe</h3>

		</div>
		<?php
			}
			else{
		?>

        <div class="card">
			<div class="card-body">
				<table class="table table-success table-bordered table-striped">
					<thead style="font-size:20px;">
						<tr>
							<th style="text-align: center;" > PO </th>
                            <th style="text-align: center;" > PO Title </th>
							<th style="text-align: center;" > PO Description </th>
							<th style="text-align: center;" > Action </th>
						</tr>
					</thead>
					<tbody>
						<?php

                            // $dept_name = $_GET['dept_name'];
							
							$sql_po = mysql_query("select * from program_outcome where dept_name = '$dept_name'",$con);
							while($res_po = mysql_fetch_array($sql_po))
    						{
							
					    ?>
							<tr style="font-size:18px;"> 
		    	    		    <td style="text-align: center; "><?php echo $res_po['po_code']; ?> </td>
                                <td style="text-align: center; "><?php echo $res_po['po_title']; ?> </td>

		    	    		    <td style="text-align: center;"><?php echo $res_po['po_description']; ?></td>

		    	    		    <td style="text-align: center;">
                                    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#<?php echo $res_po['po_id'] ?>ModalCenter">
										<i class="fa fa-pencil" aria-hidden="true" style="font-size: 18px"></i>
									</button>
									<a href="accreditation/delete_po.php?po_id=<?php echo $res_po['po_id']; ?>&dept_name=<?php echo $dept_name; ?>">
										<button type="button" class="btn btn-danger">
											<i class="fa fa-trash" aria-hidden="true" style="font-size: 18px"></i>
										</button>
							        </a>
									
								</td>

							</tr>
                            <!-- Modal -->
                            <div class="modal fade" id="<?php echo $res_po['po_id'] ?>ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalLongTitle">Edit Program Outcome</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="">
                                            <div class="modal-body">
                                                <fieldset class="form-group">
                                                    <label for="co">PO Code</label>
			                                    	<input class="form-control" type="text" name="po_code" id="po_code"  value="<?php echo $res_po['po_code']; ?> " placeholder="Eg: PO1" required="required" style="text-transform: uppercase">         
			                                    </fieldset>
                                                <fieldset class="form-group">
                                                    <label for="co">PO Title</label>
			                                    	<input class="form-control" type="text" name="po_title" id="po_title" value="<?php echo $res_po['po_title']; ?> " placeholder="" required="required" style="text-transform: uppercase">         
			                                    </fieldset>
			                                    <fieldset class="form-group">
			                                    	<label for="co_name">PO Description</label>
                                                    <textarea class="form-control" name="po_description" id="po_description" style="height:50px;" required><?php echo $res_po['po_description']; ?></textarea>
			                                    </fieldset>
                                            </div>
                                            <input type="hidden" name="po_id" value="<?php echo $res_po['po_id']; ?>">
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="submit" id="submit">Save changes</button>
                                            </div>
                                        </form>
                                        <?php
                                            if(isset($_POST['submit']))
                                            {
                                                $po_id = $_POST['po_id'];
                                                $po_code = strtoupper($_POST["po_code"]);
                                                $po_title = strtoupper( $_POST["po_title"]);
                                                $po_description = $_POST["po_description"];


                                                $update = mysql_query("update program_outcome set po_code='$po_code', po_title='$po_title',po_description='$po_description' WHERE po_id='$po_id'",$con);
                                                if ($update) {
                                                    echo "<script>alert('Succesfully Updated')</script>";
                                                    echo "<script>window.location.href='add_po.php?dept_name=$dept_name'</script>";
                                                } else {
                                                    echo "<script>alert('Failed to Update')</script>";
                                                    echo "<script>window.location.href='add_po.php?dept_name=$dept_name'</script>";
                                                
                                                }
                                            } 
                                        
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
						<?php
				    	    }
				    	?>
					</tbody>
				</table>
				
			</div>
			<?php 
				}			
			?>
		</div>
        
					
<?php include("includes/footer.php");?>
