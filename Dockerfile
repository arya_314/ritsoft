FROM ubuntu:14.04
RUN apt-get update && apt-get -y install nano git apache2 mysql-server php5-mysql php5 libapache2-mod-php5 php5-curl
COPY ritsoft.sql /
COPY create-and-grant.sql /
COPY startup.sh /
COPY setup.sh /
RUN apt-get -y install dos2unix
RUN dos2unix startup.sh ritsoft.sql create-and-grant.sql setup.sh
RUN chmod +x /setup.sh
RUN /setup.sh

#Setting the timezone
ENV TZ=Asia/Kolkata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

CMD chmod +x startup.sh && ./startup.sh && /bin/bash
